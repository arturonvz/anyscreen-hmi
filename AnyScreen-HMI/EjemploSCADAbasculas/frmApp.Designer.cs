﻿namespace EjemploSCADAbasculas
{
    partial class frmApp
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            panelPrincipal = new Panel();
            pictureBox1 = new PictureBox();
            rbtnInicio = new RadioButton();
            rbtnBásculas = new RadioButton();
            rbtnReportes = new RadioButton();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.BackColor = Color.FromArgb(0, 77, 77);
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 140F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(panelPrincipal, 1, 0);
            tableLayoutPanel1.Controls.Add(pictureBox1, 0, 0);
            tableLayoutPanel1.Controls.Add(rbtnInicio, 0, 1);
            tableLayoutPanel1.Controls.Add(rbtnBásculas, 0, 2);
            tableLayoutPanel1.Controls.Add(rbtnReportes, 0, 3);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 5;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 140F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new Size(539, 347);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // panelPrincipal
            // 
            panelPrincipal.BackColor = Color.White;
            panelPrincipal.Dock = DockStyle.Fill;
            panelPrincipal.Location = new Point(143, 3);
            panelPrincipal.Name = "panelPrincipal";
            tableLayoutPanel1.SetRowSpan(panelPrincipal, 5);
            panelPrincipal.Size = new Size(393, 341);
            panelPrincipal.TabIndex = 0;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.wsp1;
            pictureBox1.Location = new Point(3, 3);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(134, 134);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 1;
            pictureBox1.TabStop = false;
            // 
            // rbtnInicio
            // 
            rbtnInicio.Appearance = Appearance.Button;
            rbtnInicio.Checked = true;
            rbtnInicio.FlatAppearance.BorderColor = Color.White;
            rbtnInicio.FlatAppearance.BorderSize = 2;
            rbtnInicio.FlatAppearance.CheckedBackColor = Color.FromArgb(0, 154, 154);
            rbtnInicio.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 179, 179);
            rbtnInicio.FlatAppearance.MouseOverBackColor = Color.Teal;
            rbtnInicio.FlatStyle = FlatStyle.Flat;
            rbtnInicio.Font = new Font("Arial", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            rbtnInicio.ForeColor = Color.White;
            rbtnInicio.Image = Properties.Resources.Inicio;
            rbtnInicio.ImageAlign = ContentAlignment.MiddleLeft;
            rbtnInicio.Location = new Point(3, 143);
            rbtnInicio.Name = "rbtnInicio";
            rbtnInicio.Size = new Size(134, 34);
            rbtnInicio.TabIndex = 2;
            rbtnInicio.TabStop = true;
            rbtnInicio.Text = "          Inicio";
            rbtnInicio.UseVisualStyleBackColor = true;
            // 
            // rbtnBásculas
            // 
            rbtnBásculas.Appearance = Appearance.Button;
            rbtnBásculas.FlatAppearance.BorderColor = Color.White;
            rbtnBásculas.FlatAppearance.BorderSize = 2;
            rbtnBásculas.FlatAppearance.CheckedBackColor = Color.FromArgb(0, 154, 154);
            rbtnBásculas.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 179, 179);
            rbtnBásculas.FlatAppearance.MouseOverBackColor = Color.Teal;
            rbtnBásculas.FlatStyle = FlatStyle.Flat;
            rbtnBásculas.Font = new Font("Arial", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            rbtnBásculas.ForeColor = Color.White;
            rbtnBásculas.Image = Properties.Resources.weight_tool;
            rbtnBásculas.ImageAlign = ContentAlignment.MiddleLeft;
            rbtnBásculas.Location = new Point(3, 183);
            rbtnBásculas.Name = "rbtnBásculas";
            rbtnBásculas.Size = new Size(134, 34);
            rbtnBásculas.TabIndex = 2;
            rbtnBásculas.Text = "          Básculas";
            rbtnBásculas.UseVisualStyleBackColor = true;
            // 
            // rbtnReportes
            // 
            rbtnReportes.Appearance = Appearance.Button;
            rbtnReportes.FlatAppearance.BorderColor = Color.White;
            rbtnReportes.FlatAppearance.BorderSize = 2;
            rbtnReportes.FlatAppearance.CheckedBackColor = Color.FromArgb(0, 154, 154);
            rbtnReportes.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 179, 179);
            rbtnReportes.FlatAppearance.MouseOverBackColor = Color.Teal;
            rbtnReportes.FlatStyle = FlatStyle.Flat;
            rbtnReportes.Font = new Font("Arial", 8.25F, FontStyle.Bold, GraphicsUnit.Point);
            rbtnReportes.ForeColor = Color.White;
            rbtnReportes.Image = Properties.Resources.Historial_24px;
            rbtnReportes.ImageAlign = ContentAlignment.MiddleLeft;
            rbtnReportes.Location = new Point(3, 223);
            rbtnReportes.Name = "rbtnReportes";
            rbtnReportes.Size = new Size(134, 34);
            rbtnReportes.TabIndex = 2;
            rbtnReportes.Text = "          Reportes";
            rbtnReportes.UseVisualStyleBackColor = true;
            // 
            // frmApp
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(539, 347);
            Controls.Add(tableLayoutPanel1);
            Name = "frmApp";
            Text = "SCADA de Básculas";
            tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private Panel panelPrincipal;
        private PictureBox pictureBox1;
        private RadioButton rbtnInicio;
        private RadioButton rbtnBásculas;
        private RadioButton rbtnReportes;
    }
}